# Remove drush alias from Drupal VM Install.
# We are going to drop our own in there during the
# website project install process.
if  [ -f /home/vagrant/.drush/drupaljenkinstestvm.aliases.drushrc.php ]; then
  echo "Removing ~/.drush drupaljenkinstestvm.aliases.drushrc.php"
  rm /home/vagrant/.drush/drupaljenkinstestvm.aliases.drushrc.php
else
  echo "~/.drush drupaljenkinstestvm.aliases.drushrc.php already removed"
fi
