# ICF Olson Drupal Jenkins Test VM

This VM utilizes [Drupal VM](http://www.drupalvm.com/) for local Drupal development, built with Vagrant + Ansible.

It will install the following on an Ubuntu 12.04.05 Linux VM:

  - Apache 2.4.x
  - PHP 5.6.x
  - MySQL 5.5.x
  - Drush (latest)
  - Drupal 8.x (version defined in project)
  - Drupal Console
  - Varnish 4.x
  - Node.js 4.x
  - Memcached
  - XHProf, for profiling your code
  - XDebug, for debugging your code
  - Adminer, for accessing databases directly
  - Pimp my Log, for easy viewing of log files
  - MailHog, for catching and debugging email

Full Drupal VM documentation is available at http://docs.drupalvm.com/

## Installation

Recommended versions for software requirements below (April 2016)
* Vagrant 1.8.1
* VirtualBox 5.0.16
* Ansible 2.0.1.0

### 1- Clone ICF Olson Drupal Jenkins Test projects from Git

In a local folder where you like to keep project repos clone both the VM project and the ICF Olson Drupal Jenkins Test website project in folders that are next to each other.

```bash
git clone git@bitbucket.org:icfolson/icfolson-drupal-jenkins-test-vm.git
```
```bash
git clone git@bitbucket.org:icfolson/icfolson-drupal-jenkins-test.git
```

### 2 - Install Vagrant

Download and install [Vagrant](https://www.vagrantup.com/downloads.html).

Vagrant will automatically install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) if no providers are available (Drupal VM also works with Parallels or VMware, if you have the [Vagrant VMware integration plugin](http://www.vagrantup.com/vmware)).

### 3 - Install Ansible (Optional - Mac/Linux Only)

For faster provisioning [install Ansible](http://docs.ansible.com/intro_installation.html) on your host machine, so Drupal VM can run the provisioning steps locally instead of inside the VM.

  - **NFS on Linux**: *If NFS is not already installed on your host, you will need to install it to use the default NFS synced folder configuration. See guides for [Debian/Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-ubuntu-14-04), [Arch](https://wiki.archlinux.org/index.php/NFS#Installation), and [RHEL/CentOS](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-centos-6).*

### 4 - Build the Virtual Machine

1. Make a copy of the `example.config.yml` file
  - Copy `example.config.yml` to `config.yml`.
2. Set the local directory path where you checked out the `icfolson-drupal-jenkins-test` repo in `config.yml` (`local_path`, inside `vagrant_synced_folders`).
3. Open Terminal, `cd` to this directory (containing the `Vagrantfile` and this README file).
5. (If you have Ansible installed on Mac/Linux - *Recommended*) Run `$ sudo ansible-galaxy install -r provisioning/requirements.yml --force`.
6. Type in `vagrant up`, and let Vagrant do its magic.

**Note:** *If there are any errors during the course of running `vagrant up`, and it drops you back to your command prompt, just run `vagrant provision` to continue building the VM from where you left off. If there are still errors after doing this a few times work with the project tech lead to resolve.*


### 5 - Configure your host machine to access the VM

1. Open your browser and access [http://192.168.88.248](http://192.168.88.248). This will take you to the Drupal VM Dashboard. If this does not take you to the dashboard page, try restarting apache by SSHing into the VM.

  ```bash
  vagrant ssh
  sudo apachectl restart
  ```

  **Note:** *If you changed the `vagrant_ip` in the `config.yml` use that IP instead.*

2. [Edit your hosts file](http://www.rackspace.com/knowledge_center/article/how-do-i-modify-my-hosts-file), adding the lines provided on the dashboard page.
  - You can have Vagrant automatically configure your hosts file if you install the `hostsupdater` plugin (`vagrant plugin install vagrant-hostsupdater`). All hosts defined in `apache_vhosts` or `nginx_hosts` will be automatically managed. The `vagrant-hostmanager` plugin is also supported.
  - You can also have Vagrant automatically assign an available IP address to your VM if you install the `auto_network` plugin (`vagrant plugin install vagrant-auto_network`), and set `vagrant_ip` to `0.0.0.0` inside `config.yml`.

3. Open your browser and access [http://dashboard.jenkinstesting.loc/](http://dashboard.jenkinstesting.loc/) to ensure your hosts file edit worked.

### 6 - Install the website on the VM with Composer

1. SSH into the vm with the `vagrant ssh` command.

2. Navigate to the project directory with `cd /home/vagrant/project`

3. Run the `composer install` command.

4. Upon a successful installation of Drupal go to your browser and access [http://jenkinstesting.loc/user/login](http://jenkinstesting.loc/user/login). The default login for the admin account is `admin` for both the username and password.

## Other Notes

  - To shut down the virtual machine, enter `vagrant halt` in the Terminal in the same folder that has the `Vagrantfile`. To destroy it completely (if you want to save a little disk space, or want to rebuild it from scratch with `vagrant up` again), type in `vagrant destroy`.
  - You can change the installed version of Drupal or drush, or any other configuration options, by editing the variables within `config.yml`.
